import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkboxpaging',
  template: `
  <style>
    .example-section {
      display: flex;
      align-content: center;
      align-items: center;
      height: 60px;
    }

    .example-margin {
      margin: 0 10px 10px;
    }
  </style>
 <div class="col-xs-4" style="border:1px solid black;" >
    <div class="col-md-6" *ngFor="let names of name">
      <md-checkbox>{{names}}</md-checkbox>
    </div>
  </div>
  `
})
export class BaseWidgetCheckboxPagingComponent implements OnInit{
public name: Array<any> = ["Donald Trump", "Barack Obama", "Najib Razak", "Joko Widodo", "Kim Jong Nam", "Rodrigo Duterte","Angela Merkel","Rex Tillerson"];
   constructor() {console.log(this.name);}
  
  ngOnInit() { 
  }
}
