import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@angular/material';
import { BaseWidgetCheckboxPagingComponent } from './src/base-widget.component';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  declarations: [
	BaseWidgetCheckboxPagingComponent
  ],
  exports: [
    BaseWidgetCheckboxPagingComponent
  ]
})
export class CheckboxPagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CheckboxPagingModule,
      providers: []
    };
  }
}
